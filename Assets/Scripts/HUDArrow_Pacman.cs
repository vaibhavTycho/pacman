﻿using UnityEngine;
using System.Collections;

public class HUDArrow_Pacman : MonoBehaviour {


    RectTransform rectTransform;

    public float maxEnlargment;
    public float minEnlargment;

    public float animationVelocity;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

	// Use this for initialization
	void Start () {
        StartCoroutine(AnimationRoutine());
	}
	
	// Update is called once per frame
	void Update () {

        if (PlayerController_Pacman.instance.currentDir == Vector2.up)
        {
            rectTransform.rotation = Quaternion.Euler(0, 0, -90);
        }

        if (PlayerController_Pacman.instance.currentDir == Vector2.down)
        {
            rectTransform.rotation = Quaternion.Euler(0, 0, 90);
        }


        if (PlayerController_Pacman.instance.currentDir == Vector2.left)
        {
            rectTransform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if (PlayerController_Pacman.instance.currentDir == Vector2.right)
        {
            rectTransform.rotation = Quaternion.Euler(0, 0, 180);
        }


    }


    public IEnumerator AnimationRoutine()
    {
        while (true)
        {

            Vector3 targetScale = new Vector3(maxEnlargment, maxEnlargment, maxEnlargment);

            while(rectTransform.localScale != targetScale)
            {
                rectTransform.localScale = Vector3.MoveTowards(rectTransform.localScale, targetScale, Time.deltaTime * animationVelocity);
                yield return new WaitForEndOfFrame();
            }

            targetScale =  new Vector3(minEnlargment, minEnlargment, minEnlargment);

            while (rectTransform.localScale != targetScale)
            {
                rectTransform.localScale = Vector3.MoveTowards(rectTransform.localScale, targetScale, Time.deltaTime * animationVelocity);
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }
    }

}
