﻿using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    //--------------------------------------------------------
    // Game variables

    public static int Level = 0;
    public static int lives = 3;





    public bool oneMoreChanceUsed = false;

    public enum GameState { Init, Game, Dead, Scores, menu, game, gameover, shop }
    public GameState gameState;

    public GameObject pacman;
    public GameObject blinky;
    public GameObject pinky;
    public GameObject inky;
    public GameObject clyde;
    public GameObject ghosts; public GameGUINavigation gui;

    public static bool scared;

    public float scareLength;
    private float _timeToCalm;

    public float SpeedPerLevel;
    public Pacdot_Pacman[] dots;

    public float readyScreenDuration = 4;

    public static GameManager instance;

    void Awake()
    {

        instance = this;
    }

    void Start()
    {

        gameState = GameState.menu;
        dots = GameObject.FindObjectsOfType<Pacdot_Pacman>();

        if (Level > 0)
        {
            LevelWasLoaded();
        }
    }

    void LevelWasLoaded()
    {
        if (Level == 0) lives = 3;

        Debug.Log("Level " + Level + " Loaded!");
        ResetVariables();

        // Adjust Ghost variables!
        clyde.GetComponent<GhostMove_Pacman>().speed += Level * SpeedPerLevel;
        blinky.GetComponent<GhostMove_Pacman>().speed += Level * SpeedPerLevel;
        pinky.GetComponent<GhostMove_Pacman>().speed += Level * SpeedPerLevel;
        inky.GetComponent<GhostMove_Pacman>().speed += Level * SpeedPerLevel;
        pacman.GetComponent<PlayerController_Pacman>().speed += Level * SpeedPerLevel / 2;


        StartGame();

    }


    private void ResetVariables()
    {
        _timeToCalm = 0.0f;
        scared = false;
        PlayerController_Pacman.killstreak = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (scared && _timeToCalm <= Time.time)
            CalmGhosts();

    }

    public void ResetScene()
    {
        CalmGhosts();

        PlayerController_Pacman.instance.currentDir = Vector2.right;

        pacman.transform.position = new Vector3(15f, 11f, 0f);
        blinky.transform.position = new Vector3(15f, 20f, 0f);
        pinky.transform.position = new Vector3(14.5f, 17f, 0f);
        inky.transform.position = new Vector3(16.5f, 17f, 0f);
        clyde.transform.position = new Vector3(12.5f, 17f, 0f);

        pacman.GetComponent<PlayerController_Pacman>().ResetDestination();
        blinky.GetComponent<GhostMove_Pacman>().InitializeGhost();
        pinky.GetComponent<GhostMove_Pacman>().InitializeGhost();
        inky.GetComponent<GhostMove_Pacman>().InitializeGhost();
        clyde.GetComponent<GhostMove_Pacman>().InitializeGhost();

        gameState = GameState.Init;

        PlayerController_Pacman.instance._deadPlaying = false;
        GUIManager_Pacman.instance.inGameGUI.ShowReadyScreen();

    }

    public void ToggleScare()
    {
        if (!scared) ScareGhosts();
        else CalmGhosts();
    }

    public void ScareGhosts()
    {
        scared = true;
        blinky.GetComponent<GhostMove_Pacman>().Frighten();
        pinky.GetComponent<GhostMove_Pacman>().Frighten();
        inky.GetComponent<GhostMove_Pacman>().Frighten();
        clyde.GetComponent<GhostMove_Pacman>().Frighten();
        _timeToCalm = Time.time + scareLength;

        Debug.Log("Ghosts Scared");
    }

    public void CalmGhosts()
    {
        scared = false;
        blinky.GetComponent<GhostMove_Pacman>().Calm();
        pinky.GetComponent<GhostMove_Pacman>().Calm();
        inky.GetComponent<GhostMove_Pacman>().Calm();
        clyde.GetComponent<GhostMove_Pacman>().Calm();
        PlayerController_Pacman.killstreak = 0;
    }



    public void LoseLife()
    {
        lives--;
        gameState = GameState.Dead;
        PlayerController_Pacman.instance.Reset();
        // update UI too
        UIScript ui = GameObject.FindObjectOfType<UIScript>();
        Destroy(ui.lives[ui.lives.Count - 1]);
        ui.lives.RemoveAt(ui.lives.Count - 1);
    }

    public static void DestroySelf()
    {
        ScoreHandler_Pacman.instance.score = 0;
        Level = 0;
        lives = 3;
        Destroy(GameObject.Find("Game Manager"));
    }

    public IEnumerator GameOverCoroutine(float delay)
    {

        yield return new WaitForSeconds(delay);

        SoundsManager_Pacman.instance.PlayGameOverSound();
        gameState = GameState.gameover;



        GUIManager_Pacman.instance.ShowGameOverGUI();
        InGameGUI_Pacman.instance.gameObject.SetActive(false);

    }


    public void GameOver(float delay)
    {
        StartCoroutine(GameOverCoroutine(delay));
    }

    public void StartGame()
    {
        ResetGame();
        ScoreHandler_Pacman.instance.incrementNumberOfGames();
        GUIManager_Pacman.instance.ShowInGameGUI();
        pacman.gameObject.SetActive(true);
        ghosts.gameObject.SetActive(true);
        //GUIManager.instance.tutorialGUI.ShowIfNeverAppeared();
        //  AdNetworks.instance.ShowBanner();
        GUIManager_Pacman.instance.mainMenuGUI.gameObject.SetActive(false);
        gameState = GameState.Init;
    }

    public void ResetGame(bool resetScore = true, bool resetOneMoreChance = true)
    {
        if (resetOneMoreChance)
        {
            oneMoreChanceUsed = false;
        }



        if (resetScore)
        {
            ScoreHandler_Pacman.instance.reset();
        }
    }


}
