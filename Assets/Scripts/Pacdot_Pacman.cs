﻿using UnityEngine;
using System.Collections;

public class Pacdot_Pacman : MonoBehaviour
{

    public SpriteRenderer spriteRednerer;
    public bool taken;
    public bool isTurningPoint;
    public bool hasVerticalAdiacentDot;
    public bool hasHorizontalAdiacentDot;

    void Awake()
    {
        transform.tag = "pacdot";
        spriteRednerer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {

        if (CheckForAdiacentDot(Vector2.up) || CheckForAdiacentDot(Vector2.down))
        {
            hasVerticalAdiacentDot = true;
        }

        if(CheckForAdiacentDot(Vector2.left) || CheckForAdiacentDot(Vector2.right))
        {
            hasHorizontalAdiacentDot = true;
        }

        if(hasHorizontalAdiacentDot && hasVerticalAdiacentDot)
        {
            isTurningPoint = true;
        }

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "pacman")
        {
      

            if (!taken)
            {
                SoundsManager_Pacman.instance.EnableWakaWaka();
                ScoreHandler_Pacman.instance.increaseScore(10);
                spriteRednerer.enabled = false;
                taken = true;
            }
            else
            {
                SoundsManager_Pacman.instance.DisableWakaWaka();
            }

           

            if (isTurningPoint)
            {
                PlayerController_Pacman.instance.lastHitTurningPoint = transform;
            }

            PlayerController_Pacman.instance.lastHitDot = transform;


            foreach(Pacdot_Pacman dot in GameManager.instance.dots)
            {
                if (!dot.taken)
                {
                    return;
                }
            }

            GameGUINavigation.instance.LoadLevel();

        }
    }


    bool CheckForAdiacentDot(Vector2 direction)
    {
        // cast line from 'next to pacman' to pacman
        // not from directly the center of next tile but just a little further from center of next tile
        Vector2 pos = transform.position;
        RaycastHit2D hit = Physics2D.Linecast(pos + direction, pos);
        return hit.collider.name == "pacdot" || (hit.collider == GetComponent<Collider2D>());
    }

}
