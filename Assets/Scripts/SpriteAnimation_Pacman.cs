﻿using UnityEngine;
using System.Collections;

public class SpriteAnimation_Pacman : MonoBehaviour {

    public Sprite[] spriteAnims;
    public SpriteRenderer spriteRend;
    public float animSpeed = 0.25f;
	// Use this for initialization
	void Start () {
        StartCoroutine(StartSpriteAnim());
	}

    IEnumerator StartSpriteAnim() {
        int currentSpriteCount = 0;
        while (true) {


            try { spriteRend.sprite = spriteAnims[currentSpriteCount]; } catch {
                Debug.Log("bo");
            }
                yield return new WaitForSeconds(animSpeed);
                currentSpriteCount++;
                if (currentSpriteCount >= spriteAnims.Length) currentSpriteCount = 0;
            }

        }

    }


