﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

	public int high, score;

	public List<Image> lives = new List<Image>(3);

	public Text txt_score, txt_high, txt_level;
	
	// Use this for initialization
	void Start () 
	{
	

	    for (int i = 0; i < 3 - GameManager.lives; i++)
	    {
	        Destroy(lives[lives.Count-1]);
            lives.RemoveAt(lives.Count-1);
	    }
	}
	
	// Update is called once per frame
	void Update () 
	{

        high = ScoreHandler_Pacman.instance.highScore;

        // update score text
        score = ScoreHandler_Pacman.instance.score;
		txt_score.text = "SCORE " + score;
		txt_high.text = "BEST " + high;
	    txt_level.text = "LV " + (GameManager.Level + 1);

	}


}
