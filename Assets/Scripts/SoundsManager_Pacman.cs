﻿using UnityEngine;
using System.Collections;

public class SoundsManager_Pacman : MonoBehaviour
{
    public AudioSource FootStepsAudioSource;
    public AudioSource RopeDropAudioSource;
    public AudioSource LandAudioSource;
    public AudioSource PerfectLandAudioSource;
    public AudioSource DiamondAudioSource;
    public AudioSource GameOverAudioSource;
    public AudioSource MenuButtonAudioSource;
    public AudioSource WakaWakeAudioSource;
    public AudioSource SirenAudioSource;
    public AudioSource PacmanDeathAudioSource;
    public AudioSource PacmanIntroAudioSource;
    public AudioSource PacmanEatingSource;

    public static bool audioEnabled = true;



    public static SoundsManager_Pacman instance;

    bool cannotExecuteSound;
    float timeToBePaused = 0.1f;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void PlayAudioSource(AudioSource audioSource)
    {

        audioSource.Play();

    }

    IEnumerator CanExecuteCountDown()
    {
        cannotExecuteSound = true;
        yield return new WaitForSeconds(timeToBePaused);
        cannotExecuteSound = false;
    }


    public void PlayWalkSound()
    {
        if (!FootStepsAudioSource.isPlaying)
        {
            FootStepsAudioSource.Play();
        }
    }

    public void PauseWalkSound()
    {
        if (FootStepsAudioSource.isPlaying)
        {
            FootStepsAudioSource.Pause();
        }
    }

    public void PlayRopeDropSound()
    {
        RopeDropAudioSource.Play();
    }

    public void PlayLandingSound()
    {
        LandAudioSource.Play();
    }

    public void PlayPerfectLandingSound()
    {
        PerfectLandAudioSource.Play();
    }



    public void PlayGameOverSound()
    {
        GameOverAudioSource.Play();
    }

    public void PlayDiamondSound()
    {
        DiamondAudioSource.Play();
    }

    public void PlayMenuButtonSound()
    {
        MenuButtonAudioSource.Play();
    }

    public void PlayPacmanDeath()
    {
        PacmanDeathAudioSource.Play();
    }

    public void PlayPacmanIntro()
    {
        PacmanIntroAudioSource.Play();
    }

    public void PlayEatingSource()
    {
        if (!PacmanEatingSource.isPlaying)
        {
            PacmanEatingSource.Play();
        }
    }

    public void EnableWakaWaka()
    {
        if (WakaWakeAudioSource.gameObject.activeInHierarchy == false)
        {
            WakaWakeAudioSource.gameObject.SetActive(true);
            WakaWakeAudioSource.Play();
        }
    }

    public void DisableWakaWaka()
    {
        WakaWakeAudioSource.gameObject.SetActive(false);
    }


    public void EnableSiren()
    {
        if (SirenAudioSource.gameObject.activeInHierarchy == false)
        {
            SirenAudioSource.gameObject.SetActive(true);
            SirenAudioSource.Play();
        }
    }

    public void DisableSiren()
    {
        SirenAudioSource.gameObject.SetActive(false);
    }


}
