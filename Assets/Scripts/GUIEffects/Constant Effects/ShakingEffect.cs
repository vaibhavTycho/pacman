﻿using UnityEngine;
using System.Collections;

public class ShakingEffect : MonoBehaviour {

    Vector3 originalPosition;
    public float shakingPower;
    public float shakingVelocity;

    public RectTransform rectTransform;

    void Awake()
    {

    }

    void Start()
    {
        
    }


	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator StartEffect()
    {
        while (true)
        {
            rectTransform.anchoredPosition = originalPosition + Random.insideUnitSphere * shakingPower;
            yield return new WaitForSeconds(shakingVelocity);
        }
    }

    void OnEnable()
    {
        rectTransform = GetComponent<RectTransform>();
        originalPosition = rectTransform.anchoredPosition;
        StartCoroutine(StartEffect());
    }

}
