﻿using UnityEngine;
using System.Collections;

public class IntermittentVisibility : MonoBehaviour
{

    CanvasRenderer canvasRenderer;

    float originalAlpha;
    public float visibleDuration;
    public float hiddenDuration;


    void Awake()
    {
        canvasRenderer = GetComponent<CanvasRenderer>();
        originalAlpha = canvasRenderer.GetAlpha();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator StartEffect()
    {
        while (true)
        {
            yield return new WaitForSeconds(visibleDuration);
            canvasRenderer.SetAlpha(0);
            yield return new WaitForSeconds(hiddenDuration);
            canvasRenderer.SetAlpha(originalAlpha);
        }
    }

    void OnEnable()
    {
        StartCoroutine(StartEffect());
    }

}
