﻿using UnityEngine;
using System.Collections;

public class FadeOut : GUIEffect
{


    public float animationVelocity;
    public float originalAlpha;

    CanvasRenderer canvasRenderer;

    void Awake()
    {
        canvasRenderer = GetComponent<CanvasRenderer>();
        originalAlpha = canvasRenderer.GetAlpha();
    }

    // Use this for initialization
    void Start()
    {
        base.Start();      
        reset();
    }

    // Update is called once per frame
    public override void EffectUpdate()
    {
        
        float currentAlpha = canvasRenderer.GetAlpha();
        float lerpedAlpha = Mathf.MoveTowards(currentAlpha, 0, animationVelocity * Time.deltaTime);
        canvasRenderer.SetAlpha(lerpedAlpha);

        if(canvasRenderer.GetAlpha() == 0)
        {
            isExecuting = false;
        }

    }



    public override IEnumerator startEffect()
    {
        //canvasRenderer.SetAlpha(originalAlpha);
        isExecuting = true;
        isFinished = false;
        yield return null;
    }


    public override void reset()
    {
        canvasRenderer.SetAlpha(originalAlpha);
        isExecuting = false;
        isFinished = false;
    }

}
