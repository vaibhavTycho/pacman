﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlashScreenEffect : MonoBehaviour {
    public SpriteRenderer image;
    public static FlashScreenEffect instance;
	// Use this for initialization
	void Awake () {
        instance = this;
	}


  public  void TriggerFlashScreen() {
        StartCoroutine(flash());
    }

    IEnumerator flash()
    {
        Color originalColor = image.color;

        Color targetColor = originalColor;
        targetColor.a = 1;
        image.color = targetColor;

        float lerper = 0;
        float lerperTime = 0.5f;

        while (lerper<1)
        {

            lerper += Time.deltaTime / lerperTime;

            image.color = Color.Lerp(targetColor, originalColor, lerper);

            yield return new WaitForEndOfFrame();
        }




    }

    void OnDisable()
    {
        StopAllCoroutines();

        Color targetColor = image.color;
        targetColor.a = 0;
        image.color = targetColor;
    }



}
