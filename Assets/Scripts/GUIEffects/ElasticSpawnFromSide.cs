﻿using UnityEngine;
using System.Collections;

public class ElasticSpawnFromSide : GUIEffect
{



    public Vector3 startPoint;

    public float elasticPower;

    public Vector3 pointToReachWhenResistanceEqualPower;

    float distanceToCover;

    public Vector2 finalPosition;

    public float returningPower;

    void Awake()
    {
        

    }

    // Use this for initialization
    void Start()
    {
        //StartCoroutine(effect());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Initialize()
    {
        base.Initialize();
        rect.anchoredPosition = startPoint;
        distanceToCover = Vector3.Distance(rect.anchoredPosition, pointToReachWhenResistanceEqualPower);
    }

    public override IEnumerator startEffect()
    {
        yield return base.startEffect();
    }




    public override IEnumerator effect()
    {

        float newDistanceToCover = Vector3.Distance(rect.anchoredPosition, pointToReachWhenResistanceEqualPower);
        float newElasticPower = (elasticPower * newDistanceToCover) / distanceToCover;

        while (Mathf.Round(newElasticPower) > ((elasticPower/100)*20 ))
        {
            newDistanceToCover = Vector3.Distance(rect.anchoredPosition, pointToReachWhenResistanceEqualPower);
            newElasticPower = (elasticPower * newDistanceToCover) / distanceToCover;
            rect.anchoredPosition = Vector3.MoveTowards(rect.anchoredPosition, pointToReachWhenResistanceEqualPower, newElasticPower*Time.deltaTime*25);
            // rect.anchoredPosition = new Vector3(rect.anchoredPosition.x, rect.anchoredPosition.y - newElasticPower);
            yield return new WaitForEndOfFrame();
        }


        newElasticPower = 0;

        bool reached = false;

        while (!reached)
        {
            newElasticPower +=  returningPower* Time.deltaTime;
            rect.anchoredPosition = Vector3.MoveTowards(rect.anchoredPosition, finalPosition, newElasticPower);


            if(rect.anchoredPosition == finalPosition) { reached = true; }

            yield return null;
        }
    }


    
}
