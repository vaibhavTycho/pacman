﻿using UnityEngine;
using System.Collections;

public class FadeIn : GUIEffect
{


    public float animationVelocity;
    public float originalAlpha;

    CanvasRenderer canvasRenderer;

    void Awake()
    {
        canvasRenderer = GetComponent<CanvasRenderer>();
        originalAlpha = canvasRenderer.GetAlpha();
    }

    // Use this for initialization
    void Start()
    {
        base.Start();
        reset();
    }

    // Update is called once per frame
    public override void EffectUpdate()
    {

        float currentAlpha = canvasRenderer.GetAlpha();
        float lerpedAlpha = Mathf.MoveTowards(currentAlpha, originalAlpha, animationVelocity * Time.deltaTime);
        canvasRenderer.SetAlpha(lerpedAlpha);

        if (canvasRenderer.GetAlpha() == originalAlpha)
        {
            isExecuting = false;
        }
    }

    void OnEnable()
    {
        canvasRenderer.SetAlpha(0);
    }

    public override IEnumerator startEffect()
    {       
        isExecuting = true;
        isFinished = false;
        yield return null;
    }


    public override void reset()
    {
        canvasRenderer.SetAlpha(0);
        isExecuting = false;
        isFinished = false;
    }

}
