﻿using UnityEngine;
using System.Collections;

public class SpawnFromSide : GUIEffect
{
    public enum Side { top, bottom, left, right };
    public Side side;

    public float animationVelocity;
    public float afterMaxPointReachedAnimationVelocity;
    public Vector2 targetPoint;


    Vector2 maxTargetPoint;
    public float maxReachOffset;
    bool maxPointReached;

    public RectTransform canvasParentRect;
    public Vector2 parentCanvasSize;

    // Use this for initialization
    void Start()
    {
        base.Start();
        setCurrentPositionAsTargetPosition();
        reset();
    }

    // Update is called once per frame
   public override void EffectUpdate()
    {
        if (!maxPointReached)
        {
            rect.anchoredPosition = Vector3.MoveTowards(rect.anchoredPosition, maxTargetPoint, animationVelocity * Time.deltaTime);
            if (rect.anchoredPosition == maxTargetPoint)
            {
                maxPointReached = true;
            }
        }
        else {
            rect.anchoredPosition = Vector3.MoveTowards(rect.anchoredPosition, targetPoint, afterMaxPointReachedAnimationVelocity * Time.deltaTime);
            if (rect.anchoredPosition == targetPoint)
            {
                isExecuting = false;
                isFinished = true;
            }
        }
    }

    public void setOffScreen()
    {
        switch (side)
        {
            case Side.bottom:
                rect.localPosition = new Vector2(targetPoint.x, -parentCanvasSize.y / 2 - rect.rect.height / 2);
                maxTargetPoint.y += maxReachOffset;
                break;
            case Side.top:
                rect.localPosition = new Vector2(targetPoint.x, parentCanvasSize.y / 2 + rect.rect.height / 2);
                maxTargetPoint.y -= maxReachOffset;
                break;
            case Side.right:
                rect.localPosition = new Vector2(parentCanvasSize.x / 2 + rect.rect.width / 2, targetPoint.y);
                maxTargetPoint.x -= maxReachOffset;
                break;
            case Side.left:
                rect.localPosition = new Vector2(-parentCanvasSize.x / 2 - rect.rect.width / 2, targetPoint.y);
                maxTargetPoint.x += maxReachOffset;
                break;
        }
    }
    public override IEnumerator startEffect()
    {
        maxTargetPoint = targetPoint;
        setOffScreen();     
        isExecuting = true;
        maxPointReached = false;
        isFinished = false;
        yield return null;
    }


    public override void reset()
    {
        parentCanvasSize = new Vector2(canvasParentRect.rect.width, canvasParentRect.rect.height);
        rect = getRect();
        setOffScreen();
        isExecuting = false;
        isFinished = false;
    }



    public void setCurrentPositionAsTargetPosition()
    {
        rect = getRect();
        targetPoint = new Vector2();
        targetPoint = rect.anchoredPosition;
    }

}
