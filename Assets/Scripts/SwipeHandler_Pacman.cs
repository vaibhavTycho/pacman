﻿using UnityEngine;
using System.Collections;

public class SwipeHandler_Pacman : MonoBehaviour
{

    public bool isSwiping;
    public static SwipeHandler_Pacman instance;

    public enum SwipeAxis { X, Y };
    public enum SwipeDirection { UP, DOWN, LEFT, RIGHT };

    public float sensibility = 0.5f;


    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {

            StopAllCoroutines();
            isSwiping = true;
            StartCoroutine(SwipeRoutine(Input.mousePosition));

        }

        if (Input.GetMouseButtonUp(0))
        {
            StopAllCoroutines();       
            isSwiping = false;
        }
    }


    public IEnumerator SwipeRoutine(Vector2 startPos)
    {
        
        while (isSwiping)
        {
            Vector2 currentTouchPoint = Input.mousePosition;

            Debug.Log(Vector2.Distance(startPos, currentTouchPoint));

            if (Vector2.Distance(startPos, currentTouchPoint) > sensibility)
            {                
                ConsumeSwipe(startPos, Input.mousePosition);
                startPos = Input.mousePosition;
            }

           
            yield return null;
        }

        yield return null;
    }


    public void ConsumeSwipe(Vector2 startPos, Vector2 endPos)
    {
        SwipeAxis swipeAxis = DefineSwipeAxis(startPos, endPos);
        SwipeDirection swipeDirection = DefineSwipeDirection(startPos,endPos, swipeAxis);

        if(swipeDirection == SwipeDirection.RIGHT)
        {
            PlayerController_Pacman.instance.ReadInputAndMove(Vector2.right);
        }

        if (swipeDirection == SwipeDirection.LEFT)
        {
            PlayerController_Pacman.instance.ReadInputAndMove(Vector2.left);
        }

        if (swipeDirection == SwipeDirection.UP)
        {
            PlayerController_Pacman.instance.ReadInputAndMove(Vector2.up);
        }

        if (swipeDirection == SwipeDirection.DOWN)
        {
            PlayerController_Pacman.instance.ReadInputAndMove(Vector2.down);
        }


        Debug.Log(swipeDirection);
    }

    public SwipeAxis DefineSwipeAxis(Vector2 startPos, Vector2 endPos)
    {
        Vector2 difference = startPos - endPos;
        float distanceInX = Mathf.Abs(difference.x);
        float distanceInY = Mathf.Abs(difference.y);

        if (distanceInX > distanceInY)
        {
            return SwipeAxis.X;
        }
        else
        {
            return SwipeAxis.Y;
        }
    }

    public SwipeDirection DefineSwipeDirection(Vector2 startPos, Vector2 endPos, SwipeAxis swipeAxis)
    {


        if (swipeAxis == SwipeAxis.X)
        {
            if (startPos.x > endPos.x)
            {
                return SwipeDirection.LEFT;
            }
            else
            {
                return SwipeDirection.RIGHT;
            }
        }
        else
        {
            if (startPos.y > endPos.y)
            {
                return SwipeDirection.DOWN;
            }
            else
            {
                return SwipeDirection.UP;
            }
        }
    }


}
