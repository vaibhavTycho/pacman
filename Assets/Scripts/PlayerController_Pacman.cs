﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController_Pacman : MonoBehaviour
{

    public float speed = 0.4f;
    Vector2 _dest = Vector2.zero;
    Vector2 _nextDir = Vector2.zero;

    [Serializable]
    public class PointSprites
    {
        public GameObject[] pointSprites;
    }

    public PointSprites points;

    public static int killstreak = 0;

    // script handles
    private GameGUINavigation GUINav;
    private GameManager GM;

    public bool _deadPlaying = false;

    public Transform lastHitTurningPoint;
    public Transform lastHitDot;
    public Transform dotToReach;

    public static PlayerController_Pacman instance;

    public Vector2 currentDir;

    public Vector2 nextMoveDirection;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        GM = GameObject.Find("Game Manager").GetComponent<GameManager>();
        GUINav = GameObject.Find("UI Manager").GetComponent<GameGUINavigation>();
        currentDir = Vector2.right;
        _dest = transform.position;
    }

    void Update()
    {
        MoveToNextPacDot();
        UpdateRotation();
        // get the next direction from keyboard
        if (Input.GetAxis("Horizontal") > 0) ReadInputAndMove(Vector2.right);
        if (Input.GetAxis("Horizontal") < 0) ReadInputAndMove(-Vector2.right);
        if (Input.GetAxis("Vertical") > 0) ReadInputAndMove(Vector2.up);
        if (Input.GetAxis("Vertical") < 0) ReadInputAndMove(-Vector2.up);
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        switch (GameManager.instance.gameState)
        {
            case GameManager.GameState.Game:
                break;

            case GameManager.GameState.Dead:
                if (!_deadPlaying)
                    StartCoroutine(PlayDeadAnimation());
                break;
        }
    }


    void UpdateRotation()
    {
        if (currentDir == Vector2.up)
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        if (currentDir == Vector2.down)
        {
            transform.rotation = Quaternion.Euler(0, 0, -90);
        }


        if (currentDir == Vector2.left)
        {
            transform.rotation = Quaternion.Euler(0, 0, 180);
        }

        if (currentDir == Vector2.right)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

    }

    IEnumerator PlayDeadAnimation()
    {

        _deadPlaying = true;

        SoundsManager_Pacman.instance.DisableSiren();
        SoundsManager_Pacman.instance.DisableWakaWaka();

        yield return new WaitForSeconds(0.5f);

        SoundsManager_Pacman.instance.PlayPacmanDeath();

        GetComponent<Animator>().SetBool("Die", true);
        yield return new WaitForSeconds(1.5f);
        GetComponent<Animator>().SetBool("Die", false);
        _deadPlaying = true;



        if (GameManager.lives <= 0)
        {
            GameManager.instance.GameOver(0);
        }

        else
            GM.ResetScene();
    }



    public void ResetDestination()
    {
        _dest = new Vector2(15f, 11f);
        GetComponent<Animator>().SetFloat("DirX", 1);
        GetComponent<Animator>().SetFloat("DirY", 0);
    }

    public void ReadInputAndMove(Vector2 dir)
    {
        if (GameManager.instance.gameState == GameManager.GameState.Game)
        {

            if (Mathf.Abs(dir.x) == Mathf.Abs(currentDir.x) || Mathf.Abs(dir.y) == Mathf.Abs(currentDir.y)) // THIS MEANS THAT THE MOVE IS ON THE SAME DIRECTION (HORIZONTAL OR VERTICAL)
            {
                GameObject objectNextToPacman = FindNextObjectInInDirection(dir, transform.position);

                if (objectNextToPacman.tag == "pacdot")
                {
                    dotToReach = objectNextToPacman.transform;
                    currentDir = dir;
                }
                nextMoveDirection = Vector2.zero;
            }
            else
            {
                if (Vector2.Distance(transform.position, lastHitTurningPoint.position) < 1.0f)
                {
                    GameObject objectNextToPacman = FindNextObjectInInDirection(dir, lastHitTurningPoint.position);

                    if (objectNextToPacman.tag == "pacdot")
                    {
                        transform.position = lastHitTurningPoint.transform.position;
                        dotToReach = objectNextToPacman.transform;
                        currentDir = dir;
                    }
                }
                else
                {
                    nextMoveDirection = dir;
                }
            }
        }
    }


    public void MoveToNextPacDot()
    {

        if (dotToReach != null)
        {
            // move closer to destination
            Vector2 p = Vector2.MoveTowards(transform.position, dotToReach.position, speed * Time.deltaTime);
            transform.position = p;
        }
    }


    public Vector2 getDir()
    {
        return currentDir;
    }

    public void UpdateScore()
    {
        killstreak++;

        // limit killstreak at 4
        if (killstreak > 4) killstreak = 4;

        Instantiate(points.pointSprites[killstreak - 1], transform.position, Quaternion.identity);
        ScoreHandler_Pacman.instance.increaseScore((int)Mathf.Pow(2, killstreak) * 100);

    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "pacdot")
        {
            Pacdot_Pacman hitPacDot = other.GetComponent<Pacdot_Pacman>();

            if (hitPacDot != null)
            {
                GameObject objectNextToPacman = FindNextObjectInInDirection(currentDir, other.transform.position);
                if (objectNextToPacman.tag == "pacdot")
                {
                    dotToReach = objectNextToPacman.transform;

                }
                else
                {
                    SoundsManager_Pacman.instance.DisableWakaWaka();
                }


                if (hitPacDot.isTurningPoint && nextMoveDirection != Vector2.zero)
                {
                    objectNextToPacman = FindNextObjectInInDirection(nextMoveDirection, other.transform.position);
                    if (objectNextToPacman.tag == "pacdot")
                    {
                        dotToReach = objectNextToPacman.transform;
                        currentDir = nextMoveDirection;
                        nextMoveDirection = Vector2.zero;
                    }
                    else
                    {
                        SoundsManager_Pacman.instance.DisableWakaWaka();
                    }
                }
            }
        }
    }


    public GameObject FindNextObjectInInDirection(Vector2 dir, Vector2 pos)
    {
        // cast line from 'next to pacman' to pacman
        // not from directly the center of next tile but just a little further from center of next tile
        RaycastHit2D[] hits = Physics2D.LinecastAll(pos, pos + dir * 1000);
        RaycastHit2D hit = hits[1];
        return hit.collider.gameObject;
    }



    public void Reset()
    {
        dotToReach = null;
        lastHitDot = null;
        lastHitTurningPoint = null;
        nextMoveDirection = Vector2.zero;
    }

}
