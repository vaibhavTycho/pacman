﻿using UnityEngine;
using System.Collections;

public class Energizer : MonoBehaviour
{

    private GameManager gm;
    SpriteRenderer sr;
    public float blinkTime;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    void Start()
    {
        gm = GameObject.Find("Game Manager").GetComponent<GameManager>();
        StartCoroutine(Blink());
        if (gm == null) Debug.Log("Energizer did not find Game Manager!");

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.name == "pacman")
        {
            gm.ScareGhosts();
            Destroy(gameObject);
        }
    }

    public IEnumerator Blink()
    {
        while (true)
        {
            yield return new WaitForSeconds(blinkTime);
            sr.enabled = false;
            yield return new WaitForSeconds(blinkTime);
            sr.enabled = true;

            yield return new WaitForEndOfFrame();
        }
    }


}
