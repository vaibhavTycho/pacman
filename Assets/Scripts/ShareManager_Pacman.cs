﻿using UnityEngine;
using System.Collections;

public class ShareManager_Pacman : MonoBehaviour {

    public string shareMessage;
    public static ShareManager_Pacman instance;

    void Awake()
    {
        instance = this;
    }
    public void share()
    {
        StartCoroutine(NativeShare.ShareScreenshotWithText(shareMessage));
    }

}
