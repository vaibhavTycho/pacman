﻿using UnityEngine;
using System.Collections;

public class SelfDestroy : MonoBehaviour {

    public float SelfDestructionTime = 5;
	// Use this for initialization
	void Start () {
        StartCoroutine(SelfDestruction());
	}
IEnumerator SelfDestruction()
    {
        yield return new WaitForSeconds(SelfDestructionTime);
        Destroy(gameObject);

    }
}
