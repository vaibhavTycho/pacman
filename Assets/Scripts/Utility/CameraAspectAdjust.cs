﻿using UnityEngine;
using System.Collections;

public class CameraAspectAdjust : MonoBehaviour {
    public Camera cam;
    float aspect;
    public float aspect9 = 5.85f;
    public float aspect10 = 5.25f;
    public float topScreen9 = 0;
    public float topScreen10 = -0.5f;
    public Transform topScreen;
    // Use this for initialization
    void Start () {
     aspect= cam.aspect;


        if (aspect < 0.6f)
        {
            cam.orthographicSize = aspect9;
            SetTopScreenPositionY(topScreen9);
        }
        else {
            cam.orthographicSize = aspect10;
            SetTopScreenPositionY(topScreen10);


        }
    }

    void SetTopScreenPositionY(float value) {
        Vector3 topscreenPos = topScreen.position;
        topscreenPos.y = value;
        topScreen.position = topscreenPos;
    }


	

}
