﻿using UnityEngine;
using System.Collections;

public class GUIManager_Pacman : MonoBehaviour
{


    public static GUIManager_Pacman instance;

    public TutorialGUI_Pacman tutorialGUI;
    public PauseGUI_Pacman pauseGUI;
    public ShopGUI shopGUI;
    public GameOverGUI_Pacman gameOverGUI;
    public MainMenuGUI mainMenuGUI;
    public OneMoreChanceGUI oneMoreChanceGUI;
    public InGameGUI_Pacman inGameGUI;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        


    }

    public void ShowInGameGUI()
    {
        inGameGUI.gameObject.SetActive(true);
    }

    public void ShowGameOverGUI()
    {
        gameOverGUI.gameObject.SetActive(true);
    }

    public void HideGameOverGUI()
    {
        gameOverGUI.gameObject.SetActive(false);
    }


    public void ShowTutorialGUI()
    {
        tutorialGUI.Activate();
    }

    public void HideTutorialGUI()
    {
        tutorialGUI.Deactivate();
    }

    public void ShowPauseGUI()
    {
        pauseGUI.Activate();
    }

    public void HidePauseGUI()
    {
        pauseGUI.Deactivate();
    }

    public void ShowOneMoreChanceGUI()
    {
        oneMoreChanceGUI.gameObject.SetActive(true);
    }

    public void HideOneMoreChanceGUI()
    {
        oneMoreChanceGUI.gameObject.SetActive(false);
    }

    public void ShowShopGUI()
    {
        ShopHandler.instance.Activate();
    }

    public void ShowMainMenuGUI()
    {
        GameManager.instance.gameState = GameManager.GameState.menu;
        mainMenuGUI.gameObject.SetActive(true);
    }



}
