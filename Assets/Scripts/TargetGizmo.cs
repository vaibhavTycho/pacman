﻿using UnityEngine;
using System.Collections;

public class TargetGizmo : MonoBehaviour {

	public GameObject ghost;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(ghost.GetComponent<AI_Pacman>().targetTile != null)
		{
			Vector3 pos = new Vector3(ghost.GetComponent<AI_Pacman>().targetTile.x, 
										ghost.GetComponent<AI_Pacman>().targetTile.y, 0f);
			transform.position = pos;
		}
	}
}
