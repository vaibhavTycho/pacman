﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class GameOverGUI_Pacman : MonoBehaviour
{

    public Text scoreText;
    public Text highScoreText;
    public Text diamondText;


    public Text coinText;

    public Button GetCoinButton;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "" + ScoreHandler_Pacman.instance.score;
        highScoreText.text = "" + ScoreHandler_Pacman.instance.highScore;
        diamondText.text = "" + ScoreHandler_Pacman.instance.specialPoints;

    }


    public void OnGetCoinButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        GetCoinButton.interactable = false;
    }

    public void OnBallShopClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        Deactivate();
        GUIManager_Pacman.instance.ShowShopGUI();
    }

    public void OnRemoveAdsButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

      //  AdRemover.instance.BuyNonConsumable();
    }

    public void OnRestorePurchaseButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

     //   AdRemover.instance.RestorePurchases();
    }

    public void OnLeaderboardButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

    }

    public void OnShareButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        ShareManager_Pacman.instance.share();
    }

    public void OnPlayButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();
        GameManager.Level = 0;
        GameManager.lives = 3;
        Application.LoadLevel(Application.loadedLevel);
        GameManager.instance.ResetGame();

    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }


   


}
