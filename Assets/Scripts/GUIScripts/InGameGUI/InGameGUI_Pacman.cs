﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameGUI_Pacman : MonoBehaviour
{

    public static InGameGUI_Pacman instance;
    public Text scoreText;
    public Text specialPointsText;
    public GameGUINavigation gameGUINavigation;
    


    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

   

    }


    public void OnTutorialButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();
        GUIManager_Pacman.instance.ShowTutorialGUI();
    }

    public void OnPauseButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();
        GUIManager_Pacman.instance.ShowPauseGUI();
    }

    public void OnUpButtonClick()
    {
        PlayerController_Pacman.instance.ReadInputAndMove(Vector2.up);
    }


    public void OnDownButtonClick()
    {
        PlayerController_Pacman.instance.ReadInputAndMove(-Vector2.up);
    }
    public void OnLeftButtonClick()
    {
        PlayerController_Pacman.instance.ReadInputAndMove(-Vector2.right);
    }
    public void OnRighButtonClick()
    {
        PlayerController_Pacman.instance.ReadInputAndMove(Vector2.right);
    }


    public void ShowReadyScreen()
    {
        gameGUINavigation.H_ShowReadyScreen();
    }



}
