﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseGUI_Pacman : MonoBehaviour
{

    public Button soundButton;
    public Sprite soundEnabledImage;
    public Sprite soundDisabledImage;

    public void OnCloseButtonClick()
    {
        Deactivate();
    }

    public void Activate()
    {
        Time.timeScale = 0;

       gameObject.SetActive(true);
    }


    public void Update()
    {
        if (SoundsManager_Pacman.audioEnabled == false)
        {
            soundButton.image.sprite = soundDisabledImage;
        }
        else 
        {
            soundButton.image.sprite = soundEnabledImage;
        }
    }

    public void Deactivate()
    {
        Time.timeScale = 1;


        if (SoundsManager_Pacman.audioEnabled == true)
        {
            AudioListener.volume = 1;
        }
        else
        {
            AudioListener.volume = 0;

        }

        gameObject.SetActive(false);
    }

    public void onHomeButtonClick()
    {
        Time.timeScale = 1;
        GameManager.Level = 0;
        GameManager.lives = 3;

        Deactivate();
        Application.LoadLevel(Application.loadedLevel);
        GameManager.instance.ResetGame();
    }

    public void OnSoundButtonClick()
    {
        if (SoundsManager_Pacman.audioEnabled == false)
        {
            SoundsManager_Pacman.audioEnabled = true;
        }
        else
        {
            SoundsManager_Pacman.audioEnabled = false;
        }
    }


}
