﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuGUI : MonoBehaviour {

    public static MainMenuGUI instance;

    public Text highscoreText;
    public Text gamesPlayedText;

    string originalHighScoreText;
    string originalGamesPlayedText;
    void Awake()
    {
        instance = this;
        originalHighScoreText = highscoreText.text;
       // originalGamesPlayedText = gamesPlayedText.text;
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        highscoreText.text = originalHighScoreText + ScoreHandler_Pacman.instance.highScore;
       // gamesPlayedText.text = originalGamesPlayedText + ScoreHandler.instance.numberOfGames;
    }

    public void OnShopButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        gameObject.SetActive(false);
        GUIManager_Pacman.instance.ShowShopGUI();
    }

    public void OnPlayButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        GameManager.instance.StartGame();
    }

    public void OnRateButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        RateManager.instance.rateGame();
    }

}
