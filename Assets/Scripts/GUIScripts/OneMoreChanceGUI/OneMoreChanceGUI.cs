﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class OneMoreChanceGUI : MonoBehaviour {



    void Awake()
    {
        
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Activate()
    {
        gameObject.SetActive(true);
    }

    void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void OnOneMoreChanceButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

      //  UnityRewardAds.instance.ShowRewardedAd(HandleShowResult);
        Deactivate();
    }

    public void OnGameOverButtonClick()
    {
        SoundsManager_Pacman.instance.PlayMenuButtonSound();

        Deactivate();
        GameManager.instance.oneMoreChanceUsed = true;
        GameManager.instance.GameOver(0);
    }


   

}
